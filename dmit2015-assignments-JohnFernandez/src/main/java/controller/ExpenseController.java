package controller;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import ca.nait.dmit.domain.Expense;
import helper.JSFHelper;

@Named
@ViewScoped
public class ExpenseController implements Serializable {
	private static final long serialVersionUID = 1L;

	private Expense currentExpense = new Expense();

	public Expense getCurrentExpense() {
		return currentExpense;
	}

	public void setCurrentExpense(Expense currentExpense) {
		this.currentExpense = currentExpense;
	}

	public void addExpense() {

		String message = currentExpense.toString();
		JSFHelper.addInfoMessage(message);
	}

}
