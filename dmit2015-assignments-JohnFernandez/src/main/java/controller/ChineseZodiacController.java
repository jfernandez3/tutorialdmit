package controller;

import java.io.Serializable;
import java.util.Calendar;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import ca.nait.dmit.domain.ChineseZodiac;
import helper.JSFHelper;

@Named
@ViewScoped
public class ChineseZodiacController implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int birthYear = Calendar.getInstance().get(Calendar.YEAR);
	private String animalImageURL = "/resources/images/chinese_zodiac.jpg";
	
	public int getBirthYear() {
		return birthYear;
	}
	public void setBirthYear(int birthYear) {
		this.birthYear = birthYear;
	}
	public String getAnimalImageURL() {
		return animalImageURL;
	}
	public void setAnimalImageURL(String animalImageURL) {
		this.animalImageURL = animalImageURL;
	}
	
	public void calculate()
	{
		String message = String.format(birthYear + " is the year of the " + ChineseZodiac.animal(birthYear));
		JSFHelper.addInfoMessage(message);
		
		if(ChineseZodiac.animal(birthYear) == "rat")
		{
			animalImageURL = "/resources/images/zodiac_" + ChineseZodiac.animal(birthYear) + ".jpg";
			
		}
		else if(ChineseZodiac.animal(birthYear) == "ox")
		{
			animalImageURL = "/resources/images/zodiac_" + ChineseZodiac.animal(birthYear) + ".jpg";
		}
		else if(ChineseZodiac.animal(birthYear) == "tiger")
		{
			animalImageURL = "/resources/images/zodiac_" + ChineseZodiac.animal(birthYear) + ".jpg";
		}
		else if(ChineseZodiac.animal(birthYear) == "rabbit")
		{
			animalImageURL = "/resources/images/zodiac_" + ChineseZodiac.animal(birthYear) + ".jpg";
		}
		else if(ChineseZodiac.animal(birthYear) == "dragon")
		{
			animalImageURL = "/resources/images/zodiac_" + ChineseZodiac.animal(birthYear) + ".jpg";
		}
		else if(ChineseZodiac.animal(birthYear) == "snake")
		{
			animalImageURL = "/resources/images/zodiac_" + ChineseZodiac.animal(birthYear) + ".jpg";
		}
		else if(ChineseZodiac.animal(birthYear) == "horse")
		{
			animalImageURL = "/resources/images/zodiac_" + ChineseZodiac.animal(birthYear) + ".jpg";
		}
		else if(ChineseZodiac.animal(birthYear) == "goat")
		{
			animalImageURL = "/resources/images/zodiac_" + ChineseZodiac.animal(birthYear) + ".jpg";
		}
		else if(ChineseZodiac.animal(birthYear) == "monkey")
		{
			animalImageURL = "/resources/images/zodiac_" + ChineseZodiac.animal(birthYear) + ".jpg";
		}
		else if(ChineseZodiac.animal(birthYear) == "rooster")
		{
			animalImageURL = "/resources/images/zodiac_" + ChineseZodiac.animal(birthYear) + ".jpg";
		}
		else if(ChineseZodiac.animal(birthYear) == "dog")
		{
			animalImageURL = "/resources/images/zodiac_" + ChineseZodiac.animal(birthYear) + ".jpg";
		}
		else if(ChineseZodiac.animal(birthYear) == "pig")
		{
			animalImageURL = "/resources/images/zodiac_" + ChineseZodiac.animal(birthYear) + ".jpg";
		}
	}
}
