package controller;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import ca.nait.dmit.domain.BMI;
import helper.JSFHelper;

@Named
@ViewScoped
public class BmiController implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private BMI bmiBean = new BMI();

	public BMI getBmiBean() {
		return bmiBean;
	}

	public void setBmiBean(BMI bmiBean) {
		this.bmiBean = bmiBean;
	}
	
	public void calculate()
	{
		String message = String.format("Your BMI is " + Math.round(bmiBean.bmi() * 100) / 100.0 + ", you are " + bmiBean.bmiCategory());
		bmiBean.bmi();
		if(bmiBean.bmiCategory() == "normal")
			JSFHelper.addInfoMessage(message);
		if(bmiBean.bmiCategory() == "underweight" || bmiBean.bmiCategory() == "overweight")
			JSFHelper.addWarningMessage(message);
		if(bmiBean.bmiCategory() == "obese")
			JSFHelper.addFatalMessage(message);
	}
}
