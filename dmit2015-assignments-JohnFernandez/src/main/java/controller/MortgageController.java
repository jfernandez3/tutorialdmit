package controller;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.HorizontalBarChartModel;
import org.primefaces.model.chart.ChartSeries;

import ca.nait.dmit.domain.Loan;
import ca.nait.dmit.domain.LoanSchedule;
import helper.JSFHelper;

@Named
@ViewScoped
public class MortgageController implements Serializable {
	private static final long serialVersionUID = 1L;

	private Loan currentLoan = new Loan();
	private LoanSchedule[] loanScheduleTable = new LoanSchedule[currentLoan.getAmortizationPeriod()];
	private BarChartModel loanChart = new BarChartModel();

	public Loan getCurrentLoan() {
		return currentLoan;
	}

	public void setCurrentLoan(Loan currentLoan) {
		this.currentLoan = currentLoan;
	}

	public LoanSchedule[] getLoanScheduleTable() {
		return loanScheduleTable;
	}

	public void setLoanScheduleTable(LoanSchedule[] loanScheduleTable) {
		this.loanScheduleTable = loanScheduleTable;
	}

	public BarChartModel getLoanChart() {
		return loanChart;
	}

	public void setLoanChart(BarChartModel loanChart) {
		this.loanChart = loanChart;
	}

	public void calculate() {
		String message = String.format("Your monthly payment is " + currentLoan.getMonthlyPayment());
		JSFHelper.addInfoMessage(message);

		ChartSeries years = new ChartSeries();
		loanChart.setLegendPosition("ne");
		loanChart.setTitle("Mortgage Amortization Paydown");
		years.setLabel(currentLoan.getMortgageAmount() + " at " + currentLoan.getAnnualInterestRate() + " for " 
		+ currentLoan.getAmortizationPeriod() + " years");
		loanScheduleTable = currentLoan.getLoanScheduleArray();

		for (int index = 0; index < currentLoan.getAmortizationPeriod(); index++) {
				years.set(loanScheduleTable[index].getPaymentNumber(),
						loanScheduleTable[index * 12].getRemainingBalance());
		}
		loanChart.addSeries(years);

	}

}
