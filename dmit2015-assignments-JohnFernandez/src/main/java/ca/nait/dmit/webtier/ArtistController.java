package ca.nait.dmit.webtier;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import ca.nait.dmit.businesstier.ArtistService;
import ca.nait.dmit.entity.Artist;

//@Named //access using jsf-el #{name}
//@RequestScoped //@SessionScoped @ViewScoped @ApplicationScoped
@Model //shortcut for @Named and @requestscoped
public class ArtistController {
	
	@Inject
	private ArtistService artistService;
	private List<Artist> artists;
	@Inject
	private Logger logger;
	
	@PostConstruct
	public void init() {
		logger.infov("Fetch artists");
		artists = artistService.getArtists();
	}
	public List<Artist> getArtists() {
		return artists;
	}
	
	
}
