package ca.nait.dmit.domain;

import java.text.NumberFormat;
import java.util.Date;
import javax.validation.constraints.*;

public class Expense {

	@Past(message = "Date cannot be in the future.")
	private java.util.Date date;

	@Size(min = 3, max = 255, message = "Description must be between 3 and 255")
	@NotNull(message = "Description is required.")
	private String description;

	@Min(value = 1, message = "Amount must be greater or equal to $1.00")
	private double amount;

	public Expense() {
		super();
		// TODO Auto-generated constructor stub
	}

	public java.util.Date getDate() {
		return date;
	}

	public void setDate(java.util.Date date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Expense(Date date, String description, double amount) {
		super();
		this.date = date;
		this.description = description;
		this.amount = amount;
	}

	@Override
	public String toString() {
		NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
		return "Successfully added the following expense: date = " + date + ", description = " + description + ", amount = "
				+ currencyFormat.format(amount);
	}
}
