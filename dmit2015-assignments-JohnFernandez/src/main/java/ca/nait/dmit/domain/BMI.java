package ca.nait.dmit.domain;

/**
 * This class is use to calculate a person's body mass index (BMI) and their BMI
 * Category.
 * 
 * @author John Fernandez
 * @version 2015.01.16
 */

public class BMI {
	private int weight;
	private int height;

	public BMI() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BMI(int weight, int height) {
		super();
		this.weight = weight;
		this.height = height;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	/**
	 * Calculate the body mass index (BMI) using the weight and height of the
	 * person. The BMI of a person is calculated using the formula: BMI = 700 *
	 * weight / (height * height) where weight is in pounds and height is in
	 * inches.
	 * 
	 * @return the body mass index (BMI) value of the person
	 */

	public double bmi() {
		return (703 * weight) / (Math.pow(height, 2));
	}

	/**
	 * Determines the BMI Category of the person using their BMI value and
	 * comparing it against the following table.
	 * <table>
	 * <thead>
	 * <tr>
	 * <th>BMI Range</th>
	 * <th>BMI Category</th>
	 * </tr>
	 * </thead> <tbody>
	 * <tr>
	 * <td>< 18.5</td>
	 * <td>underweight</td>
	 * </tr>
	 * <tr>
	 * <td>>= 18.5 and < 25</td>
	 * <td>normal</td>
	 * </tr>
	 * <tr>
	 * <td>>= 25 and < 30</td> 3.
	 * http://localhost:8080/javawockee-web/sections/dmit2015/winter2015/ass...
	 * 3 of 4 1/16/2015 6:42 PM
	 * <td>overweight</td>
	 * </tr>
	 * <tr>
	 * <td>>= 30</td>
	 * <td>obese</td>
	 * </tr>
	 * </tbody>
	 * </table>
	 *
	 * @return one of following: underweight, normal, overweight, obese.
	 */

	public String bmiCategory() {
		String userCategory;
		double bmiWeight = bmi();

		if (bmiWeight < 18.5)
			userCategory = "underweight";
		else if (bmiWeight >= 18.5 && bmiWeight <= 24.9)
			userCategory = "normal";
		else if (bmiWeight >= 25 && bmiWeight <= 29.9)
			userCategory = "overweight";
		else
			userCategory = "obese";

		return userCategory;
	}
}
