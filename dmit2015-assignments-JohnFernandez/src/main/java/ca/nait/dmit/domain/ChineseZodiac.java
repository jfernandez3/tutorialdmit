package ca.nait.dmit.domain;

/**
 * This class determines the year animal of the user from the Chinese zodiac.
 * Author: John Fernandez
 * Date: 2015,09,18
 */

public class ChineseZodiac {
	
	/**
	 * This class returns a string containing the user's animal from the Chinese zodiac.
	 */
	
	public static String animal(int birthYear)
	{
		String userAnimal = "none";
		
		birthYear = (birthYear - 1900) % 12;
		
		if(birthYear == 0)
			userAnimal = "rat";
		if(birthYear == 1)
			userAnimal = "ox";
		if(birthYear == 2)
			userAnimal = "tiger";
		if(birthYear == 3)
			userAnimal = "rabbit";
		if(birthYear == 4)
			userAnimal = "dragon";
		if(birthYear == 5)
			userAnimal = "snake";
		if(birthYear == 6)
			userAnimal = "horse";
		if(birthYear == 7)
			userAnimal = "goat";
		if(birthYear == 8)
			userAnimal = "monkey";
		if(birthYear == 9)
			userAnimal = "rooster";
		if(birthYear == 10)
			userAnimal = "dog";
		if(birthYear == 11)
			userAnimal = "pig";
		
		return userAnimal;
	}
	
}
