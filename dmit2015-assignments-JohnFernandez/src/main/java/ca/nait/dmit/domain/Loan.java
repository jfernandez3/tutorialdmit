package ca.nait.dmit.domain;

import java.util.ArrayList;

public class Loan {
	private double mortgageAmount;
	private double annualInterestRate;
	private int amortizationPeriod;

	public Loan() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Loan(double mortgageAmount, double annualInterestRate, int amortizationPeriod) {
		super();
		this.mortgageAmount = mortgageAmount;
		this.annualInterestRate = annualInterestRate;
		this.amortizationPeriod = amortizationPeriod;
	}

	public double getMortgageAmount() {
		return mortgageAmount;
	}

	public void setMortgageAmount(double mortgageAmount) {
		this.mortgageAmount = mortgageAmount;
	}

	public double getAnnualInterestRate() {
		return annualInterestRate;
	}

	public void setAnnualInterestRate(double annualInterestRate) {
		this.annualInterestRate = annualInterestRate;
	}

	public int getAmortizationPeriod() {
		return amortizationPeriod;
	}

	public void setAmortizationPeriod(int amortizationPeriod) {
		this.amortizationPeriod = amortizationPeriod;
	}

	public double getMonthlyPayment() {
		double monthlyPayment;
		double functionA = Math.pow((1.0 + annualInterestRate / 200), (1.0 / 6.0));
		double functionB = Math.pow(functionA, -12.0 * amortizationPeriod);

		monthlyPayment = (mortgageAmount * ((functionA - 1)) / (1 - functionB));
		
		return RoundDecimal(monthlyPayment);
	}


	public LoanSchedule[] getLoanScheduleArray() {
		double remainingBalance = mortgageAmount;
		double monthlyPercentageRate;
		double interestPaid;
		double principalPaid;
		int paymentNumber = 0;
		double monthlyPayment = getMonthlyPayment();

		LoanSchedule[] loanScheduleArray = new LoanSchedule[amortizationPeriod * 12];

		for (int index = 1; index <= amortizationPeriod * 12; index++) {
			paymentNumber++;
			interestPaid = RoundDecimal(
					(Math.pow(1.0 + annualInterestRate / 200.0, (1.0 / 6.0)) - 1.0) * remainingBalance);
			principalPaid = RoundDecimal(getMonthlyPayment() - interestPaid);

			if (remainingBalance < monthlyPayment) {
				principalPaid = remainingBalance;
			}

			remainingBalance = RoundDecimal(remainingBalance - principalPaid);

			LoanSchedule loanScheduleArray2 = new LoanSchedule(paymentNumber, RoundDecimal(interestPaid), 
					RoundDecimal(principalPaid),
					RoundDecimal(remainingBalance));
			loanScheduleArray[index - 1] = loanScheduleArray2;
		}
		return loanScheduleArray;
	}

	public static double RoundDecimal(double decimal)
	{
		return Math.round(decimal * 100.0) / 100.0;
	}
}
