package ca.nait.dmit.businesstier;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import ca.nait.dmit.eistier.ArtistDao;
import ca.nait.dmit.entity.Artist;

@Stateless
public class ArtistService {
	
	@Inject //initialize/create
	private ArtistDao artistDao; //not needed anymore = new ArtistDao();
	
	//debugging
	@Inject
	private Logger logger;
	
	@PostConstruct
	public void retreiveAllArtist() {
		artists = artistDao.findAll();
		logger.infov("retrieved all artists {0}", artists.size());
	}
	
	private List<Artist> artists;
	
	public List<Artist> getArtists() {
		return artists;
	}
	
}
