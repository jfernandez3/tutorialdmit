package ca.nait.dmit.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ca.nait.dmit.domain.BMI;

/**
 * Servlet implementation class BMIServlet
 */
@WebServlet("/BMIServlet")
public class BMIServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BMIServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at:
		// ").append(request.getContextPath());
		
		String Weight = request.getParameter("WeightTextBox");
		String Height = request.getParameter("HeightTextBox");
		
		BMI bmiObject = new BMI();
		
		response.setContentType("text/html");
		if (Weight.isEmpty()) {
			response.getWriter().println("Please enter your weight.");
		} else if (Height.isEmpty()) {
			response.getWriter().println("Please enter your height.");
		} else {
			BMI bmiHeight = new BMI();
			bmiHeight.setHeight(Integer.parseInt(Height));
			bmiHeight.setWeight(Integer.parseInt(Weight));
			String bmiType = bmiObject.bmiCategory();
			double bmi = bmiObject.bmi();
			response.getWriter().println("Your BMI is <strong>" + Math.round(bmi * 100) / 100.0 + 
					"</strong> " + "you are <strong>" + bmiType + "</strong>.");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
