package ca.nait.dmit.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ca.nait.dmit.domain.Loan;
import ca.nait.dmit.domain.LoanSchedule;

/**
 * Servlet implementation class LoanServlet
 */
@WebServlet("/LoanServlet")
public class LoanServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoanServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at:
		// ").append(request.getContextPath());
		String mortgageAmount = request.getParameter("MortgageAmountText");
		String annualInterestRate = request.getParameter("AnnualInterestRateText");
		String amortizationPeriod = request.getParameter("AmortizationPeriodText");
		
		Loan loanSchedule = new Loan();

		response.setContentType("text/html");
		if (mortgageAmount.isEmpty())
			response.getWriter().println("Please enter the mortgage amount.");
		else if (annualInterestRate.isEmpty())
			response.getWriter().println("Please enter the annual interest rate.");
		else if (amortizationPeriod.isEmpty())
			response.getWriter().println("Please enter the amortization period.");
		else {
			Loan loan = new Loan();
			loan.setAmortizationPeriod(Integer.parseInt(amortizationPeriod));
			loan.setAnnualInterestRate(Double.parseDouble(annualInterestRate));
			loan.setMortgageAmount(Double.parseDouble(mortgageAmount));
			
			LoanSchedule[] loanArray = loanSchedule.getLoanScheduleArray();	
			double monthlyPayment = loanSchedule.getMonthlyPayment();			

			response.getWriter().println("Your monthly payment amount is <strong>" + monthlyPayment + "</strong>");
			
			response.getWriter().println("<table>");
			response.getWriter().println("<tr>");
			response.getWriter().println("<td>" + "Payment Number" + "--------" + "</td>");
			response.getWriter().println("<td>" + "Interest Paid   " + "--------" + "</td>");
			response.getWriter().println("<td>" + "Principal Paid   " + "--------" + "</td>");
			response.getWriter().println("<td>" + "Remaining Balance" + "</td>");
			response.getWriter().println("</tr>");
			
			for(int index = 0; index < loanArray.length; index++)
			{			
				response.getWriter().println("<tr>");
				response.getWriter().println("<td>" + loanArray[index].getPaymentNumber() + "</td>");
				response.getWriter().println("<td>" + loanArray[index].getInterestPaid() + "</td>"); 
				response.getWriter().println("<td>" + loanArray[index].getPrincipalPaid() + "</td>");
				response.getWriter().println("<td>" + loanArray[index].getRemainingBalance() + "</td>");				
				response.getWriter().println("</tr>");
				
				//underlines
				response.getWriter().println("<tr>");
				response.getWriter().println("<td>");
				response.getWriter().println("<hr>");
				response.getWriter().println("</td>");
				response.getWriter().println("<td>");
				response.getWriter().println("<hr>");
				response.getWriter().println("</td>");
				response.getWriter().println("<td>");
				response.getWriter().println("<hr>");
				response.getWriter().println("</td>");
				response.getWriter().println("<td>");
				response.getWriter().println("<hr>");
				response.getWriter().println("</td>");
				response.getWriter().println("</tr>");
			}
			response.getWriter().println("</table>");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
