package ca.nait.dmit.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ca.nait.dmit.domain.ChineseZodiac;

/**
 * Servlet implementation class ChineseZodiacServlet
 */
@WebServlet("/ChineseZodiacServlet")
public class ChineseZodiacServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChineseZodiacServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		String birthYear = request.getParameter("YearTextBox");
		String zodiac;
		
		//images
		String ratImage = "resources/images/zodiac_rat.jpg";
		String oxImage = "resources/images/zodiac_ox.jpg";
		String tigerImage = "resources/images/zodiac_tiger.jpg";
		String rabbitImage = "resources/images/zodiac_rabbit.jpg";
		String dragonImage = "resources/images/zodiac_dragon.jpg";
		String snakeImage = "resources/images/zodiac_snake.jpg";
		String horseImage = "resources/images/zodiac_horse.jpg";
		String goatImage = "resources/images/zodiac_goat.jpg";
		String monkeyImage = "resources/images/zodiac_monkey.jpg";
		String roosterImage = "resources/images/zodiac_rooster.jpg";
		String dogImage = "resources/images/zodiac_dog.jpg";
		String pigImage = "resources/images/zodiac_pig.jpg";	
		String htmlText;
		
		response.setContentType("text/html");
		if(birthYear.isEmpty())
		{
			response.getWriter().println("Please enter the year of your birth.");
		}
		else
		{
			zodiac = ChineseZodiac.animal(Integer.parseInt(birthYear));
			response.getWriter().println(birthYear + " is the year of the <strong>" + zodiac + "</strong>");
			
			if(zodiac == "rat")
			{
				htmlText = "<p><img src='" + ratImage + "' /></p>";
				response.getWriter().println(htmlText);	
			}
			else if(zodiac == "ox")
			{
				htmlText = "<p><img src='" + oxImage + "' /></p>";
				response.getWriter().println(htmlText);
			}
			else if(zodiac == "tiger")
			{
				htmlText = "<p><img src='" + tigerImage + "' /></p>";
				response.getWriter().println(htmlText);
			}
			else if(zodiac == "rabbit")
			{
				htmlText = "<p><img src='" + rabbitImage + "' /></p>";
				response.getWriter().println(htmlText);
			}
			else if(zodiac == "dragon")
			{
				htmlText = "<p><img src='" + dragonImage + "' /></p>";
				response.getWriter().println(htmlText);
			}
			else if(zodiac == "snake")
			{
				htmlText = "<p><img src='" + snakeImage + "' /></p>";
				response.getWriter().println(htmlText);
			}
			else if(zodiac == "horse")
			{
				htmlText = "<p><img src='" + horseImage + "' /></p>";
				response.getWriter().println(htmlText);
			}
			else if(zodiac == "goat")
			{
				htmlText = "<p><img src='" + goatImage + "' /></p>";
				response.getWriter().println(htmlText);
			}
			else if(zodiac == "monkey")
			{
				htmlText = "<p><img src='" + monkeyImage + "' /></p>";
				response.getWriter().println(htmlText);
			}
			else if(zodiac == "rooster")
			{
				htmlText = "<p><img src='" + roosterImage + "' /></p>";
				response.getWriter().println(htmlText);
			}
			else if(zodiac == "dog")
			{
				htmlText = "<p><img src='" + dogImage + "' /></p>";
				response.getWriter().println(htmlText);
			}
			else if(zodiac == "pig")
			{
				htmlText = "<p><img src='" + pigImage + "' /></p>";
				response.getWriter().println(htmlText);
			}
		}
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
