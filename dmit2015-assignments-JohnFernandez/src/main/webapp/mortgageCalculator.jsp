<%@page
	import="ca.nait.dmit.domain.Loan, ca.nait.dmit.domain.LoanSchedule, java.text.NumberFormat"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Mortgage</title>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<form method="post" action="mortgageCalculator.jsp">
			<div class="jumbotron">
				<h1>Mortgage Payment Calculator</h1>
			</div>
			<%
			Loan loanSchedule = new Loan();
				if (request.getMethod().equalsIgnoreCase("POST")) {
					// copy from LoanServlet.java the code from the doPost method to here and
					// 1) remove any line reference to "response.getContentType".
					// 2) replace all occurrence of "out" with "out".
					String mortgageAmount = request.getParameter("MortgageAmountText");
					String annualInterestRate = request.getParameter("AnnualInterestRateText");
					String amortizationPeriod = request.getParameter("AmortizationPeriodText");

					response.setContentType("text/html");
					if (mortgageAmount.isEmpty())
						out.println("Please enter the mortgage amount.");
					else if (annualInterestRate.isEmpty())
						out.println("Please enter the annual interest rate.");
					else if (amortizationPeriod.isEmpty())
						out.println("Please enter the amortization period.");
					else {
						Loan loan = new Loan();
						loan.setAmortizationPeriod(Integer.parseInt(amortizationPeriod));
						loan.setAnnualInterestRate(Double.parseDouble(annualInterestRate));
						loan.setMortgageAmount(Double.parseDouble(mortgageAmount));
						LoanSchedule[] loanArray = loanSchedule.getLoanScheduleArray();
						double monthlyPayment = loanSchedule.getMonthlyPayment();

						out.println("Your monthly payment amount is <strong>" + monthlyPayment + "</strong>");

						out.println("<table>");
						out.println("<tr>");
						out.println("<td>" + "Payment Number" + "--------" + "</td>");
						out.println("<td>" + "Interest Paid   " + "--------" + "</td>");
						out.println("<td>" + "Principal Paid   " + "--------" + "</td>");
						out.println("<td>" + "Remaining Balance" + "</td>");
						out.println("</tr>");

						for (int index = 0; index < loanArray.length; index++) {
							out.println("<tr>");
							out.println("<td>" + loanArray[index].getPaymentNumber() + "</td>");
							out.println("<td>" + loanArray[index].getInterestPaid() + "</td>");
							out.println("<td>" + loanArray[index].getPrincipalPaid() + "</td>");
							out.println("<td>" + loanArray[index].getRemainingBalance() + "</td>");
							out.println("</tr>");

							//underlines
							out.println("<tr>");
							out.println("<td>");
							out.println("<hr>");
							out.println("</td>");
							out.println("<td>");
							out.println("<hr>");
							out.println("</td>");
							out.println("<td>");
							out.println("<hr>");
							out.println("</td>");
							out.println("<td>");
							out.println("<hr>");
							out.println("</td>");
							out.println("</tr>");
						}
						out.println("</table>");
					}
				}
			%>
			<div class="form-group">
				<label for="birthyear">Mortgage Amount: </label> <input type="text"
					name="MortgageAmountText" class="form-control"
					placeholder="Enter the mortgage amount." />
			</div>

			<div class="form-group">
				<label for="annualinterestrate">Annual Interest Rate: </label> <input
					type="text" name="AnnualInterestRateText" class="form-control"
					placeholder="Enter the annual interest rate." />
			</div>

			<div class="form-group">
				<label for="birthyear">Amortization Period: </label> <input
					type="text" name="AmortizationPeriodText" class="form-control"
					placeholder="Enter the amortization period." />
			</div>

			<input type="submit" value="submit" class="btn btn-primary">
		</form>
	</div>
</body>
</html>


<!--What is a Servlet and its role?
	A program that is activated to process the input submitted by the user.
	
What is JavaServer Pages (JSP) used for?
	A program that allows developers to write java code into html web pages.

What can't you do with JSP that you can do with Servlets?
	Servlets allow you to attach security constraints on an application's servlets.
-->


