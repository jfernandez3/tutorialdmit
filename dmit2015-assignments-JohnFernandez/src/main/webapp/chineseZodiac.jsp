<%@page import="ca.nait.dmit.domain.ChineseZodiac"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Chinese Zodiac</title>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<form method="post" action="chineseZodiac.jsp">
			<div class="jumbotron">
				<h1>Chinese Zodiac Calculator</h1>
			</div>
			<%
		if (request.getMethod().equalsIgnoreCase("POST")) {
			// copy from ChineseZodiacServlet.java the code from the doPost method to here and
			// 1) remove any line reference to "response.getContentType".
			// 2) replace all occurrence of "out" with "out".
			String birthYear = request.getParameter("YearTextBox");
			String zodiac;

			//images
			String ratImage = "resources/images/zodiac_rat.jpg";
			String oxImage = "resources/images/zodiac_ox.jpg";
			String tigerImage = "resources/images/zodiac_tiger.jpg";
			String rabbitImage = "resources/images/zodiac_rabbit.jpg";
			String dragonImage = "resources/images/zodiac_dragon.jpg";
			String snakeImage = "resources/images/zodiac_snake.jpg";
			String horseImage = "resources/images/zodiac_horse.jpg";
			String goatImage = "resources/images/zodiac_goat.jpg";
			String monkeyImage = "resources/images/zodiac_monkey.jpg";
			String roosterImage = "resources/images/zodiac_rooster.jpg";
			String dogImage = "resources/images/zodiac_dog.jpg";
			String pigImage = "resources/images/zodiac_pig.jpg";
			String htmlText;

			response.setContentType("text/html");
			if (birthYear.isEmpty()) {
				out.println("Please enter the year of your birth.");
			} else {
				zodiac = ChineseZodiac.animal(Integer.parseInt(birthYear));
				out.println(birthYear + " is the year of the <strong>" + zodiac + "</strong>");

				if (zodiac == "Rat") {
					htmlText = "<p><img src='" + ratImage + "' /></p>";
					out.println(htmlText);
				} else if (zodiac == "Ox") {
					htmlText = "<p><img src='" + oxImage + "' /></p>";
					out.println(htmlText);
				} else if (zodiac == "Tiger") {
					htmlText = "<p><img src='" + tigerImage + "' /></p>";
					out.println(htmlText);
				} else if (zodiac == "Rabbit") {
					htmlText = "<p><img src='" + rabbitImage + "' /></p>";
					out.println(htmlText);
				} else if (zodiac == "Dragon") {
					htmlText = "<p><img src='" + dragonImage + "' /></p>";
					out.println(htmlText);
				} else if (zodiac == "Snake") {
					htmlText = "<p><img src='" + snakeImage + "' /></p>";
					out.println(htmlText);
				} else if (zodiac == "Horse") {
					htmlText = "<p><img src='" + horseImage + "' /></p>";
					out.println(htmlText);
				} else if (zodiac == "Goat") {
					htmlText = "<p><img src='" + goatImage + "' /></p>";
					out.println(htmlText);
				} else if (zodiac == "Monkey") {
					htmlText = "<p><img src='" + monkeyImage + "' /></p>";
					out.println(htmlText);
				} else if (zodiac == "Rooster") {
					htmlText = "<p><img src='" + roosterImage + "' /></p>";
					out.println(htmlText);
				} else if (zodiac == "Dog") {
					htmlText = "<p><img src='" + dogImage + "' /></p>";
					out.println(htmlText);
				} else if (zodiac == "Pig") {
					htmlText = "<p><img src='" + pigImage + "' /></p>";
					out.println(htmlText);
				}
			}
		}
	%>
			<div class="form-group">
				<label for="birthyear">Birth Year:</label> <input type="text"
					name="YearTextBox" class="form-control"
					placeholder="Enter your birth year." />
			</div>
			<input type="submit" value="submit" class="btn btn-primary">
		</form>
	</div>
</body>
</html>