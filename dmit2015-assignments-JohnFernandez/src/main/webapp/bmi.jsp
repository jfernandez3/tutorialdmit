<%@page import="ca.nait.dmit.domain.BMI, java.text.NumberFormat"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>BMI</title>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>

<body>

	<div class="container">
		<form method="post" action="bmi.jsp">
			<div class="jumbotron">
				<h1>Body Mass Calculator</h1>
			</div>
			<%
					BMI bmiBean = new BMI();
			
				if (request.getMethod().equalsIgnoreCase("POST")) {
					String Weight = request.getParameter("WeightTextBox");
					String Height = request.getParameter("HeightTextBox");

					response.setContentType("text/html");
					if (Weight.isEmpty()) {
						out.println("Please enter your weight.");
					} else if (Height.isEmpty()) {
						out.println("Please enter your height.");
					} else {
						BMI bmiHeight = new BMI();
						bmiHeight.setHeight(Integer.parseInt(Height));
						bmiHeight.setWeight(Integer.parseInt(Weight));
						String bmiType = bmiBean.bmiCategory();
						double bmi = bmiBean.bmi();
						out.println("Your BMI is <strong>" + Math.round(bmi * 100) / 100.0 + "</strong> "
								+ "you are <strong>" + bmiType + "</strong>.");
					}
				}
			%>

			<div class="form-group">
				<label for="weight">Weight:</label> <input type="text"
					name="WeightTextBox" class="form-control"
					placeholder="Enter your weight in pounds." />
			</div>

			<div class="form-group">
				<label for="height">Height:</label> <input type="text"
					name="HeightTextBox" class="form-control"
					placeholder="Enter your height in inches." />
			</div>
			<input type="submit" value="submit" class="btn btn-primary">
		</form>
	</div>
</body>
</html>