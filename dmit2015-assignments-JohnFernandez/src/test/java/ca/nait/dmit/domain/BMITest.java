package ca.nait.dmit.domain;

import static org.junit.Assert.*;

import org.junit.Test;

public class BMITest {
	
	@Test
	public void testBmiUnderWeight() {
		BMI bmiBean = new BMI(100,66);
		assertEquals(16.1, bmiBean.bmi(), 0.05);
		assertEquals("underweight", bmiBean.bmiCategory());
	}
	
	@Test
	public void testBmiNormal() {
		BMI bmiBean = new BMI(140,66);
		assertEquals(22.6, bmiBean.bmi(), 0.05);
		assertEquals("normal", bmiBean.bmiCategory());
	}
	
	@Test
	public void testBmiOverWeight() {
		BMI bmiBean = new BMI(175,66);
		assertEquals(28.2, bmiBean.bmi(), 0.05);
		assertEquals("overweight", bmiBean.bmiCategory());
	}
	
	@Test
	public void testBmiObese() {
		BMI bmiBean = new BMI(200,66);
		assertEquals(32.3, bmiBean.bmi(), 0.05);
		assertEquals("obese", bmiBean.bmiCategory());
	}
	
	@Test// 98420/4624 = 21.284 normal
	public void testMyBMI() {
		BMI bmiBean = new BMI(140,68);
		assertEquals(21.28, bmiBean.bmi(), 0.05);
		assertEquals("normal", bmiBean.bmiCategory());
	}
}
