package ca.nait.dmit.domain;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ChineseZodiacTest.class, LoanTest.class })
public class AllTests {

}

//moodle questions

/**
 * 1. What is the purpose of unit testing and what can you test for?
 *  Unit testing is performed to test parts of a software to ensure that each part works as expected. A programmer
 *  can test for operators, functions and methods to see if the test case produces the desired results.
 *
 * 2. What is a unit testing framework used for?
 *  The unit testing framework is used create code for testing.
 * 
 * 3. How does Eclipse know if a Java class contains JUnit test methods?
 *  Eclipse can tell if a class has test methods if the block of code starts with the @Test annotation.
 *  
 *  4. Describe the difference between the test result failure and error.
 *   An error occurs when an exception is thrown when running the program meanwhile a failure is when
 *   the test that was executed did not meet the expected result.
 *  
 *  5. What is the difference between a JUnit Test Case and a JUnit Test Suite?
 *   A JUnit Test Case is included inside a JUnit Test Suite. The JUnit Test Suite is composed
 *   of several JUnit Test Cases.
 */